CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

[Nested Entity Reference Formatter](https://www.drupal.org/project/nested_entity_reference_formatter)
module offers a powerful and flexible solution for managing complex
relationships between Drupal entities. It introduces a new configurable field
formatter for entity reference fields, enabling you to display nested fields and
apply different formatters to them dynamically using AJAX.

This module is particularly useful when you need to display related entities and
their respective fields, providing an efficient way to handle multi-level entity
relationships. It offers seamless navigation and exploration of related content
while maintaining a clean and organized presentation.

For instance, you have a Drupal website with Taxonomy terms for categorizing
your content and Paragraphs to create modular content structures.
Each entity reference field has various related fields, such as Weight,
Description, Term ID, text fields, images, and even other entity reference
fields. The Nested Entity Reference Formatter module allows you to select and
display these related fields using appropriate formatters, providing a rich and
informative representation of structure.

Key features of the Nested Entity Reference Formatter module include:

- Configurable field formatter: Easily select the desired nested field and
  formatter for related entity-reference fields, giving you complete control
  over the display of your content.

- Infinite nesting: Supports an unlimited number of nesting levels, allowing you
  to display complex data structures with ease.

- Extensible: Works with custom entities and fields, making it a versatile
  solution for various use cases.

- Improved content management: By simplifying the display of nested entity
  references, content editors can focus on creating and managing content without
  worrying about complex data structures.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install the File Log module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

1. Create entity reference field:

* On `admin/structure` choose **Content types**.
* Choose content type entity reference field should be added to, for example:
  *Article*, and click **Manage fields** and then **+ Add field**.

2. Choose field formatter for your entity reference field:

* Open *Manage display* page for the selected content type.
* Select your reference field and set the formatter *Nested Entity Reference*
  from the dropdown.
* In formatter settings select a field that you want to display
  then choose a formatter that related to this field type.
* Click **Update** and then **Save**.

MAINTAINERS
-----------

* Oleksandr Mokliak (mokys) - https://www.drupal.org/u/mokys
