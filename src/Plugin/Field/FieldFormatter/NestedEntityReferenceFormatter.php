<?php

namespace Drupal\nested_entity_reference_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FormatterPluginManager;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityDisplayBase;

/**
 * Plugin implementation of the 'nested entity reference' formatter.
 *
 * @FieldFormatter(
 *   id = "nested_entity_reference",
 *   label = @Translation("Nested Entity Reference"),
 *   description = @Translation("Display the nested entity reference formmater."),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class NestedEntityReferenceFormatter extends EntityReferenceFormatterBase {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The formatter manager.
   *
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected $formatterManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The number of times this formatter allows rendering the same entity.
   *
   * @var int
   */
  public const RECURSIVE_RENDER_LIMIT = 20;

  /**
   * An array of counters for the recursive rendering protection.
   *
   * Each counter takes into account all the relevant information about the
   * field and the referenced entity that is being rendered.
   *
   * @var array
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter::viewElements()
   */
  protected static $recursiveRenderDepth = [];

  /**
   * Constructs a EntityReferenceEntityFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FormatterPluginManager $formatter_manager
   *   The formatter manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(string $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, string $label, string $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, FormatterPluginManager $formatter_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->formatterManager = $formatter_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.formatter'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['reference_fields' => []] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $parents = [];
    // Check if the settings form is being called from the Views UI.
    if ($this->isViewsUi($form, $form_state)) {
      // Pass the appropriate parents for the Views UI scenario.
      $parents = $form_state->getUserInput()['_triggering_element_name'] ?? [];
      if ($parents) {
        $parents = explode('[', str_replace(']', '', $parents));
      }
    }
    // Pass parents array for the Field UI scenario.
    if ($triggering_element = $form_state->getTriggeringElement()) {
      $parents = $triggering_element['#parents'];
    }

    return $this->settingsFormWithParents($form, $form_state, $parents);
  }

  /**
   * Checks if the settings form is being called from the Views UI.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if the settings form is being called from the Views UI,
   *   FALSE otherwise.
   */
  protected function isViewsUi(array $form, FormStateInterface $form_state): bool {
    // Check if the form has the Views UI specific element.
    return $form_state->getBuildInfo()['form_id'] === 'views_ui_config_item_form';
  }

  /**
   * Builds the settings form with the specified parents.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $parents
   *   The parents array.
   *
   * @return array
   *   The settings form array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function settingsFormWithParents(array $form, FormStateInterface $form_state, array $parents) {
    $elements = parent::settingsForm($form, $form_state);
    $target_bundles = $this->getTargetBundles();
    $options = $this->getPluginOptions();

    $elements['reference_fields'] = [
      '#open' => FALSE,
      '#title' => $this->t('Fields'),
      '#tree' => TRUE,
      '#required' => TRUE,
    ];

    foreach ($options as $bundle => $fields) {
      $css_id = Html::getUniqueId('edit-' . $bundle . '-wrapper');
      $elements['reference_fields'][$bundle] = [
        '#type' => 'fieldset',
        '#title' => $target_bundles[$bundle],
        '#prefix' => '<div id="' . $css_id . '">',
        '#suffix' => '</div>',
      ];

      $default_values = $this->getDefaultValues($bundle, $parents, $form_state);
      $reference_field = $default_values['field'] ?? '';

      array_unshift($fields, $this->t('None'));

      $elements['reference_fields'][$bundle]['field'] = [
        '#title' => $this->t('Field'),
        '#type' => 'select',
        '#default_value' => $reference_field,
        '#options' => $fields,
        '#op' => 'field',
        '#ajax' => [
          'callback' => [$this, 'fieldAjaxCallback'],
          'wrapper' => $css_id,
          'effect' => 'fade',
          'field' => $reference_field,
        ],
      ];

      if ($reference_field) {
        $settings = $default_values['formatter'] ?? [];
        $this->getFormatterForm($elements['reference_fields'][$bundle], $form_state, $bundle, $reference_field, $settings);
      }
    }

    $elements['#after_build'][] = [$this, 'clearValues'];

    return $elements;
  }

  /**
   * Clears dependent form values when the formatter change the value.
   *
   * Implemented as an #after_build callback because #after_build runs before
   * validation, allowing the values to be cleared early enough to prevent the
   * "Illegal choice" error.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The updated form element.
   */
  public static function clearValues(array $element, FormStateInterface $form_state) {
    // Get the triggering element.
    $triggering_element = $form_state->getTriggeringElement();
    // If there's no triggering element, return the element as is.
    if (!$triggering_element) {
      return $element;
    }

    // Get the trigger operation.
    $trigger_name = $triggering_element['#op'] ?? NULL;

    // If the trigger operation is 'field' or 'type'.
    if ($trigger_name === 'field' || $trigger_name === 'type') {
      // Get the parents of the triggering element.
      $parents = $triggering_element['#parents'];

      // Remove the last element from the parents array.
      $parents = array_slice($parents, 0, -1);

      // Get the user input and values from the form state.
      $input = &$form_state->getUserInput();
      $values = &$form_state->getValues();

      // Add 'formatter' to the parents array.
      $parents = array_merge($parents, ['formatter']);

      // Unset the values in the input and values arrays using the
      // updated parents.
      NestedArray::unsetValue($input, $parents);
      NestedArray::unsetValue($values, $parents);

      // Remove the last element from the parents array.
      array_pop($parents);

      // Get the field from the parents array.
      $field = array_pop($parents);

      // Get the plugin ID if available.
      $plugin_id = $element['reference_fields'][$field]['formatter']['settings_wrapper']['settings']['#plugin_id'] ?? NULL;

      // If the plugin ID is available, update the value of the 'type' element.
      if ($plugin_id) {
        $element['reference_fields'][$field]['formatter']['type']['#value'] = $plugin_id;
      }
    }

    return $element;
  }

  /**
   * Gets the default value or updated value from the form state.
   *
   * @param string $bundle
   *   The bundle of the field.
   * @param array $parents
   *   An array of parent keys that point to the part of the submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The default value or updated value from form state.
   */
  protected function getDefaultValues(string $bundle, array $parents, FormStateInterface $form_state): array {
    // If no parents are provided, get the setting for the reference_fields.
    if (!$parents) {
      return $this->getSetting('reference_fields')[$bundle] ?? [];
    }

    $default_values = [];
    // Reverse the parents array to search for the bundle from the end.
    $reversed_array = array_reverse($parents);
    // Find the index of the bundle in the reversed array.
    $index = array_search($bundle, $reversed_array, TRUE);

    // If the bundle is found in the parents array,
    // slice the parents array to get the part up to the found index.
    if ($index) {
      $index = count($parents) - $index;
      $parents = array_slice($parents, 0, $index);
      // Get the values from the form state.
      $default_values = NestedArray::getValue($form_state->getValues(), $parents);
      // If no values are found, get the user input from the form state.
      if (!$default_values) {
        $default_values = NestedArray::getValue($form_state->getUserInput(), $parents);
      }
    }

    // If still no default values are found, get the setting for the settings.
    if (!$default_values) {
      $default_values = $this->getSetting('reference_fields')[$bundle] ?? [];
    }

    return (array) $default_values;
  }

  /**
   * Shows formatter settings form for the selected field.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $bundle
   *   The field bundle.
   * @param string $reference_field
   *   The reference field name.
   * @param array $settings
   *   The formatter settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFormatterForm(array &$form, FormStateInterface $form_state, string $bundle, string $reference_field, array $settings) {
    // Get the target type and field definitions.
    $target_type = $this->getFieldSetting('target_type');
    $definitions = $this->entityFieldManager->getFieldDefinitions($target_type, $bundle);
    $field_definition = $definitions[$reference_field];
    // Get the field name.
    $field_name = $this->fieldDefinition->getName();

    // Create the formatter form element.
    $form['formatter'] = [
      '#tree' => TRUE,
      '#process' => [
        [$this, 'formatterSettingsProcessCallback'],
      ],
      '#bundle' => $bundle,
      '#field' => $reference_field,
    ];

    // Add the label field to the formatter form element.
    $form['formatter']['label'] = [
      '#type' => 'select',
      '#title' => $this->t('Label'),
      '#options' => [
        'above' => $this->t('Above'),
        'inline' => $this->t('Inline'),
        'hidden' => '- ' . $this->t('Hidden') . ' -',
        'visually_hidden' => '- ' . $this->t('Visually Hidden') . ' -',
      ],
      '#default_value' => $settings['label'] ?? '',
    ];

    // Create a wrapper ID for the formatter settings AJAX callback.
    $wrapper_id = Html::getId($target_type . '-' . $bundle . '-' . $reference_field . '-' . $field_name . '-formatter-settings-wrapper-' . md5($this->fieldDefinition->getUniqueIdentifier()));

    // Add the formatter type field to the formatter form element.
    $form['formatter']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Formatter'),
      '#options' => $this->getApplicablePluginOptions($field_definition),
      '#required' => TRUE,
      '#op' => 'type',
      '#default_value' => $settings['type'] ?? [],
      '#validated' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'formatterSettingsAjaxCallback'],
        'wrapper' => $wrapper_id,
      ],
    ];

    // Add the formatter settings wrapper to the formatter form element.
    $form['formatter']['settings_wrapper'] = [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];

  }

  /**
   * Render API callback: builds the formatter settings elements.
   *
   * @param array $element
   *   The form element to process. Properties used:.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form element.
   */
  public function formatterSettingsProcessCallback(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($formatter = $this->getFormatter($element, $form_state)) {
      $element['settings_wrapper']['settings'] = $formatter->settingsForm($complete_form, $form_state);
      $element['settings_wrapper']['settings']['#parents'] = array_merge($element['#parents'], ['settings']);
      $element['settings_wrapper']['settings']['#plugin_id'] = $formatter->getPluginId();
      $element['settings_wrapper']['third_party_settings'] = $this->thirdPartySettingsForm($formatter, $this->fieldDefinition, $complete_form, $form_state);
      $element['settings_wrapper']['third_party_settings']['#parents'] = array_merge($element['#parents'], ['third_party_settings']);
    }
    return $element;
  }

  /**
   * Adds the formatter third party settings forms.
   *
   * @param \Drupal\Core\Field\FormatterInterface $plugin
   *   The formatter.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $form
   *   The (entire) configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The formatter third party settings form.
   */
  protected function thirdPartySettingsForm(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, array $form, FormStateInterface $form_state) {
    $settings_form = [];
    $this->moduleHandler->invokeAllWith(
      'field_formatter_third_party_settings_form',
      function (callable $hook, string $module) use (&$settings_form, $plugin, $field_definition, $form, $form_state) {
        $settings_form[$module] = $hook(
          $plugin,
          $field_definition,
          EntityDisplayBase::CUSTOM_MODE,
          $form,
          $form_state,
        );
      }
    );
    return $settings_form;
  }

  /**
   * Gets the formatter object.
   *
   * @param array $element
   *   The element representing the formatter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Field\FormatterInterface|null
   *   The formatter object.
   */
  protected function getFormatter(array $element, FormStateInterface $form_state) {
    $parents = $element['#parents'];
    $bundle = $element['#bundle'];
    $field_name = $element['#field'];
    // Use the processed values, if available.
    $configuration = NestedArray::getValue($form_state->getValues(), $parents);
    if (empty($configuration)) {
      // Next check the raw user input.
      $configuration = NestedArray::getValue($form_state->getUserInput(), $parents);
    }
    if (empty($configuration)) {
      // If no user input exists, use the default values.
      $default_fields = $this->getSetting('reference_fields') ?? [];
      $configuration = $default_fields[$bundle]['formatter'] ?? [];
    }

    $target_type = $this->getFieldSetting('target_type');
    $definitions = $this->entityFieldManager->getFieldDefinitions($target_type, $bundle);

    // Set options.
    $options = [
      'configuration' => $configuration,
      'field_definition' => $definitions[$field_name],
      'view_mode' => EntityDisplayBase::CUSTOM_MODE,
      'prepare' => TRUE,
    ];

    // In some cases, fields do not have any 'default_formatter'
    // formatter as example UUID.
    if (!empty($configuration) && empty($configuration['type'])) {
      return NULL;
    }

    return $this->formatterManager->getInstance($options);
  }

  /**
   * Render API callback: gets formatter setting element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form items.
   */
  public function formatterSettingsAjaxCallback(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $parents = array_merge($parents, ['settings_wrapper']);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Render API callback: gets the field elements.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form items.
   */
  public function fieldAjaxCallback(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Returns an array of applicable formatter options for a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   An array of applicable formatter options.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see \Drupal\field_ui\Form\EntityDisplayFormBase::getApplicablePluginOptions()
   */
  protected function getApplicablePluginOptions(FieldDefinitionInterface $field_definition): array {
    $options = $this->formatterManager->getOptions($field_definition->getType());
    $applicable_options = [];
    foreach ($options as $option => $label) {
      $plugin_class = DefaultFactory::getPluginClass($option, $this->formatterManager->getDefinition($option));
      if ($plugin_class::isApplicable($field_definition)) {
        $applicable_options[$option] = $label;
      }
    }
    return $applicable_options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $settings = $this->getSetting('reference_fields') ?? [];
    $bundles = $this->getTargetBundles();
    foreach ($settings as $bundle => $bundle_settings) {
      $field_name = $bundle_settings['field'] ?? '';
      $summary[] = [
        '#type' => 'markup',
        '#markup' => '<strong>' . $bundles[$bundle] . '</strong>',
      ];
      $target_type = $this->getFieldSetting('target_type');

      if ($field_name) {
        $summary[] = [
          '#type' => 'markup',
          '#markup' => sprintf("%s <em>%s</em>", t('Field name:'), $field_name),
        ];
        $configuration = $bundle_settings['formatter'] ?? [];
        $definitions = $this->entityFieldManager->getFieldDefinitions($target_type, $bundle);

        if (!empty($configuration) && empty($configuration['type'])) {
          continue;
        }

        $formatter = $this->formatterManager->getInstance([
          'configuration' => $configuration,
          'field_definition' => $definitions[$field_name],
          'view_mode' => EntityDisplayBase::CUSTOM_MODE,
          'prepare' => TRUE,
        ]);

        if ($formatter) {
          $formatter_summary = $formatter->settingsSummary();
          $summary = array_merge($summary, $formatter_summary);
        }
      }
      else {
        $summary[] = $this->t('None');
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSetting('reference_fields');
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      // Due to render caching and delayed calls, the viewElements() method
      // will be called later in the rendering process through a '#pre_render'
      // callback, so we need to generate a counter that takes into account
      // all the relevant information about this field and the referenced
      // entity that is being rendered.
      $recursive_render_id = $items->getFieldDefinition()
        ->getTargetEntityTypeId()
        . $items->getFieldDefinition()->getTargetBundle()
        . $items->getName()
        // We include the referencing entity, so we can render default images
        // without hitting recursive protections.
        . $items->getEntity()->id()
        . $entity->getEntityTypeId()
        . $entity->id();

      if (isset(static::$recursiveRenderDepth[$recursive_render_id])) {
        static::$recursiveRenderDepth[$recursive_render_id]++;
      }
      else {
        static::$recursiveRenderDepth[$recursive_render_id] = 1;
      }

      // Protect ourselves from recursive rendering.
      if (static::$recursiveRenderDepth[$recursive_render_id] > static::RECURSIVE_RENDER_LIMIT) {
        $this->loggerFactory->get('entity')
          ->error('Recursive rendering detected when rendering entity %entity_type: %entity_id, using the %field_name field on the %bundle_name bundle. Aborting rendering.', [
            '%entity_type' => $entity->getEntityTypeId(),
            '%entity_id' => $entity->id(),
            '%field_name' => $items->getName(),
            '%bundle_name' => $items->getFieldDefinition()->getTargetBundle(),
          ]);
        return $elements;
      }
      if (empty($settings[$entity->bundle()]['field'])) {
        continue;
      }
      $bundle_settings = $settings[$entity->bundle()];
      if (!empty($entity->{$bundle_settings['field']})) {
        $elements[$delta] = $entity->{$bundle_settings['field']}->view($bundle_settings['formatter']);
      }
    }

    return $elements;
  }

  /**
   * Provides a list of possible target bundles for this field.
   *
   * @return array
   *   The list of target bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTargetBundles(): array {
    // Get the target entity type and handler settings from the field settings.
    $target_type = $this->getFieldSetting('target_type');
    $handler_settings = $this->getFieldSetting('handler_settings');

    // Get the entity type definition.
    $entity_type = $this->entityTypeManager->getDefinition($target_type);

    // Initialize an empty array to store the target bundles.
    $target_bundles = [];

    // If the handler settings do not specify any target bundles,
    // use all available bundles.
    $bundles = empty($handler_settings['target_bundles']) ? array_keys($this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) : $handler_settings['target_bundles'];
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($target_type);
    // Get all the bundle information for the target entity type.
    foreach ($bundles as $bundle_name) {
      $target_bundles[$bundle_name] = $bundle_info[$bundle_name]['label'];
    }

    return $target_bundles;
  }

  /**
   * Returns an array of options for a field.
   *
   * @return array
   *   An array of options for each applicable formatter in the target bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getPluginOptions(): array {
    // Get target bundles and target entity type.
    $target_bundles = $this->getTargetBundles();
    $target_type = $this->getFieldSetting('target_type');

    // Initialize an empty options array.
    $options = [];

    // Iterate through target bundles.
    foreach ($target_bundles as $bundle_name => $bundle_label) {
      // Get field definitions for the target entity type and bundle.
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($target_type, $bundle_name);

      // Iterate through field definitions.
      foreach ($field_definitions as $field_definition) {
        // Check if there are applicable formatters for the field.
        $applicable_formatters = $this->getApplicablePluginOptions($field_definition);

        // If there are applicable formatters, add the field to the
        // options array.
        if (!empty($applicable_formatters)) {
          if (!isset($options[$bundle_name])) {
            $options[$bundle_name] = [];
          }
          $options[$bundle_name][$field_definition->getName()] = $field_definition->getLabel();
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that have a view
    // builder.
    $target_type = $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type');
    return \Drupal::entityTypeManager()
      ->getDefinition($target_type)
      ->hasViewBuilderClass();
  }

}
