<?php

namespace Drupal\Tests\nested_entity_reference_formatter\FunctionalJavascript;

use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\node\Entity\Node;

/**
 * Test the Nested Entity Reference Formatter.
 *
 * @group nested_entity_reference_formatter
 */
class NestedEntityReferenceFormatterTest extends WebDriverTestBase {

  /**
   * Modules to enable for this test.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'nested_entity_reference_formatter',
    'taxonomy',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The Entity View Display for the article node type.
   *
   * @var \Drupal\Core\Entity\Entity\EntityViewDisplay
   */
  protected $display;

  /**
   * The node entity.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $node;

  /**
   * The vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  protected $vocabulary;

  /**
   * The name of the test node type to create.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $contentType;

  /**
   * The name of the field to use in this test.
   *
   * @var string
   */
  protected $fieldName = '';

  /**
   * The name of the ref field to use in this test.
   *
   * @var string
   */
  protected $fieldRef = '';

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $type_name = mb_strtolower($this->randomMachineName(8));

    $this->contentType = $this->drupalCreateContentType([
      'name' => $type_name,
      'type' => $type_name,
    ]);

    $user = $this->drupalCreateUser([
      'create ' . $type_name . ' content',
      'edit own ' . $type_name . ' content',
      'access content',
      'administer content types',
      'administer node fields',
      'administer node form display',
      'administer node display',
      'bypass node access',
    ]);

    $this->drupalLogin($user);

    $this->createVocabulary();
    $this->createFields();

    // Add a term to the vocabulary.
    $term = Term::create([
      'name' => 'Nested entity reference formatter',
      'description' => $this->randomMachineName(),
      'vid' => $this->vocabulary->id(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);

    $term->save();

    $this->node = Node::create([
      'title' => 'test Node',
      'type' => $this->contentType->id(),
      $this->fieldName => ['target_id' => $term->id()],
      'status' => NodeInterface::PUBLISHED,
    ]);

    $this->node->save();

    // Update term.
    $term = Term::load($term->id());
    $term->{$this->fieldRef}->setValue($this->node->id());
    $term->Save();

  }

  /**
   * Create fields to be used in node creation.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFields(): void {
    $this->fieldName = mb_strtolower($this->randomMachineName(8));
    $this->fieldRef = mb_strtolower($this->randomMachineName(8));

    FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();

    FieldStorageConfig::create([
      'field_name' => $this->fieldRef,
      'entity_type' => 'taxonomy_term',
      'type' => 'entity_reference',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'node',
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => $this->fieldRef,
      'entity_type' => 'taxonomy_term',
      'type' => 'entity_reference',
      'bundle' => $this->vocabulary->id(),
      'settings' => [
        'handler' => 'default:node',
        'handler_settings' => [
          'target_bundles' => [
            $this->contentType->id() => $this->contentType->id(),
          ],
          'sort' => [
            'field' => '_none',
            'direction' => 'asc',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => '',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'bundle' => $this->contentType->id(),
      'settings' => [
        'handler' => 'default:taxonomy_term',
        'handler_settings' => [
          'target_bundles' => [
            $this->vocabulary->id() => $this->vocabulary->id(),
          ],
          'sort' => [
            'field' => 'name',
            'direction' => 'asc',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => '',
        ],
      ],
    ])->save();
  }

  /**
   * Create vocabulary to be used in reference creation.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createVocabulary(): void {
    $this->vocabulary = Vocabulary::create([
      'name' => mb_strtolower($this->randomMachineName(8)),
      'vid' => mb_strtolower($this->randomMachineName()),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);

    $this->vocabulary->save();
  }

  /**
   * Set single nested reference on formatter on Display Manager of Node Entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException|
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testSingleNestedReferenceFormatter(): void {
    $entityTypeManager = $this->container->get('entity_type.manager');

    $this->display = $entityTypeManager->getStorage('entity_view_display')
      ->load("node.{$this->contentType->id()}.default");

    $this->display->setComponent($this->fieldName,
      ['type' => 'nested_entity_reference']
    )->save();

    // Display the "Manage display" screen and check that the expected formatter
    // is selected.
    $this->drupalGet("admin/structure/types/manage/{$this->contentType->id()}/display");

    $session = $this->getSession();
    $assert_session = $this->assertSession();
    $page = $session->getPage();

    $field_region = $page->findField("fields[{$this->fieldName}][region]");
    $button_save = $page->findButton('Save');

    // Check the region, it must be content.
    self::assertEquals('content', $field_region->getValue());

    // Check formatter type.
    $field_test_format_type = $page->findField("fields[{$this->fieldName}][type]");
    self::assertEquals('nested_entity_reference', $field_test_format_type->getValue());

    $field_test_settings = $page->find('css', "input[name='{$this->fieldName}_settings_edit']");
    // Open the settings form for the test field.
    $field_test_settings->click();
    $assert_session->assertWaitOnAjaxRequest();

    $field_test_ref_field_name = $page->findField("fields[{$this->fieldName}][settings_edit_form][settings][reference_fields][{$this->vocabulary->id()}][field]");
    $field_test_ref_field_name->setValue('name');
    $assert_session->assertWaitOnAjaxRequest();
    $page->findButton('Update')->click();
    $assert_session->assertWaitOnAjaxRequest();

    // Save the result.
    $button_save->click();

    self::assertTrue($field_test_settings->isVisible());

    $this->drupalGet("node/{$this->node->id()}");

    // Check that taxonomy name is present on the page.
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('Nested entity reference formatter');
  }

  /**
   * Set multiple nested reference on formatter on Display Manager of Node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException|
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testMultipleNestedReferenceFormatter(): void {
    $entityTypeManager = $this->container->get('entity_type.manager');

    $this->display = $entityTypeManager->getStorage('entity_view_display')
      ->load("node.{$this->contentType->id()}.default");

    $this->display->setComponent($this->fieldName, [
      'type' => 'nested_entity_reference',
      'settings' => [
        'reference_fields' => [
          $this->vocabulary->id() => [
            'field' => $this->fieldRef,
            'formatter' => [
              'type' => 'nested_entity_reference',
              'settings' => [
                'reference_fields' => [
                  $this->contentType->id() => [
                    'field' => '0',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ])->save();

    // Display the "Manage display" screen and check that the expected formatter
    // is selected.
    $this->drupalGet("admin/structure/types/manage/{$this->contentType->id()}/display");

    $session = $this->getSession();
    $assert_session = $this->assertSession();
    $page = $session->getPage();

    $field_region = $page->findField("fields[{$this->fieldName}][region]");
    $button_save = $page->findButton('Save');

    // Check the region, it must be content.
    self::assertEquals('content', $field_region->getValue());

    // Check formatter type.
    $field_test_format_type = $page->findField("fields[{$this->fieldName}][type]");
    self::assertEquals('nested_entity_reference', $field_test_format_type->getValue());

    $field_test_settings = $page->find('css', "input[name='{$this->fieldName}_settings_edit']");
    // Open the settings form for the test field.
    $field_test_settings->click();
    $assert_session->assertWaitOnAjaxRequest();

    $field_test_node_field_name = $page->findField("fields[{$this->fieldName}][settings_edit_form][settings][reference_fields][{$this->vocabulary->id()}][formatter][settings][reference_fields][{$this->contentType->id()}][field]");
    $field_test_node_field_name->setValue('title');
    $assert_session->assertWaitOnAjaxRequest();
    $page->findButton('Update')->click();

    // Save the result.
    $button_save->click();

    self::assertTrue($field_test_settings->isVisible());

    $this->drupalGet("node/{$this->node->id()}");

    // Check that node title is present on the page.
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('test Node');
  }

}
